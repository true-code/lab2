export const CONFNAME = "./config.json";

export const INSTRUCTIONS = `
Инструкция по использованию:
1. Зайдите в раздел загрузки данных. Загрузите данные удобным вам способом. Обратите внимание на требования к содержимому файлов.
2. Зайдите в Настройки. Укажите ссылку на канал-получатель, периодичность приглашений.
3. Вернитесь главное меню, нажмите "Начать приглашение".

Быстрые переходы по разделам:
/start - Главное меню
/settings - Меню настроек
/load - Меню загрузки
`;

export const MAIN_MENU = {
  INSTRUCTIONS: "Инструкция по использованию",
  START_MAILING: "Начать приглашение",
  END_MAILING: "Остановить приглашение",
  LOAD_DATA: "Загрузить данные для рассылки",
  SETTINGS: "Настройки",
};

export const SETTINGS_MENU = {
  PERIOD: "Установить периодичность рассылки",
  CHANNEL: "Установить канал-получатель",
  TO_MAIN_MENU: "Назад",
};

export const LOAD_DATA_MENU = {
  YANDEX: "Загрузить документ с яндекс диска",
  LOCAL: "Загрузить локальный файл",
  TO_MAIN_MENU: "Назад",
};

// для лимита в 200 человек за день
export const MIN_MIN = 8;

export const EXCEL_MIME_TYPE =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";